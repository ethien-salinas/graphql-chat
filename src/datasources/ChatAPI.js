import { DataSource } from 'apollo-datasource'

export default class ChatAPI extends DataSource {

  constructor({ store }) {
    super()
    this.store = store
  }
  initialize(config) {
    this.context = config.context
  }

  async getMessages() {
    const found = await this.store.Chat.findAll()
    return found
  }

  async addMessage({ author, comment }) {
    const chat = {
      author,
      comment
    }
    return await this.store.Chat.create(chat)
  }

}
