require('dotenv').config()
import { ApolloServer } from 'apollo-server'
import typeDefs from './schema'
import resolvers from './resolvers'
import createStore from './persistence/connection'
import AuthAPI from './datasources/AuthAPI'
import ChatAPI from './datasources/ChatAPI'
import UserAPI from './datasources/UserAPI'

const store = createStore()

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => ({
    authAPI: new AuthAPI({ store }),
    chatAPI: new ChatAPI({ store }),
    userAPI: new UserAPI({ store }),
  }),
  context: async ({ req, connection }) => {
    return connection
      ? connection.context
      : { token: req.headers.authorization || '' }
  }
})

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`)
})