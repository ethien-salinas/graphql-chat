import Sequelize from 'sequelize'

export default () => {

  const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './database/storage.sqlite',
    operatorsAliases: false,
    logging: false
  })

  const Users = sequelize.import('../models/users')
  const Chat = sequelize.import('../models/chatMsgs')

  return { Users, Chat }

}
