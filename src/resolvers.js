import { PubSub } from 'apollo-server'

const pubsub = new PubSub()
const CHAT_IDFORTHISCHAT = 'CHAT_IDFORTHISCHAT'

export default {

  Query: {
    getChats: async (_, __, { dataSources, token }) =>
      await dataSources.authAPI.verifyToken(token)
      && dataSources.chatAPI.getMessages(),
    login: async (_, { email, password }, { dataSources }) =>
      await dataSources.authAPI.getToken({ email, password })
  },

  Mutation: {
    addChat: async (_, { author, comment }, { dataSources, token }) => {
      return await dataSources.authAPI.verifyToken(token)
        && pubsub.publish(CHAT_IDFORTHISCHAT, { chatAdded: { author, comment } })
        && dataSources.chatAPI.addMessage({ author, comment })
    },
    signup: async (_, { name, email, password }, { dataSources }) => {
      return dataSources.userAPI.createUser({ name, email, password })
    }
  },

  Subscription: {
    chatAdded: {
      subscribe: () => pubsub.asyncIterator([CHAT_IDFORTHISCHAT])
    },
  }

}
