import { gql } from 'apollo-server'

export default gql`

  type Query {
    getChats: [Chat]
    login(email: String, password: String): String
  }
  type Mutation {
    addChat(author: String, comment: String): Chat
    signup(name: String, email: String, password: String): User
  }
  type Subscription {
    chatAdded: Chat
  }
  type Chat {
    author: String
    comment: String
  }
  type User {
    id: ID!
    name: String!
    email: String!
  }

`